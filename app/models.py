from datetime import datetime
from flask_login import UserMixin
from hashlib import md5
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login

# Association table that has no data other than foreign keys
followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)

    # Link User instances to other User instances
    followed = db.relationship(
        'User', # Right side entity of relationship
        secondary=followers, # Configures association table used for this relationship
        primaryjoin=(followers.c.follower_id == id), # Links follower with followers table
        secondaryjoin=(followers.c.followed_id == id), # Links followed with followers table
        backref=db.backref('followers', lazy='dynamic'), # How relationship will be accessed from the right side
        lazy='dynamic' # Don't run until specifically requested
    )

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size
        )

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        # Check if this User is linked to the 'user' arg
        return self.followed.filter(
            followers.c.followed_id == user.id
        ).count() > 0

    def followed_posts(self):
        # Get posts from followed users
        followed = Post.query.join(
            followers, # The 'followers' association table
            (followers.c.followed_id == Post.user_id)
        ).filter(
            followers.c.follower_id == self.id # Get only entries that have this User as a follower
        )
        # Get user's own posts
        own = Post.query.filter_by(user_id=self.id) 
        # Combine the two queries into one
        return followed.union(own).order_by(Post.timestamp.desc()) 

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))